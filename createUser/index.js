const {getToken, createUser} = require('./../graphHelper')
const secrets = require('../secrets')

module.exports = async function (context, req) {

  let tennantId = secrets.tenants.indexOf(req.query.tenant) > 0 ? req.query.tenant : secrets.tenants[0]

  let result = await getToken(tennantId).then(token => {

    let options = {
      method: 'POST',
      url: 'https://graph.microsoft.com/beta/users/',
      headers:
        {
          'Authorization': token,
          'Content-Type': 'application/json'
        },
      body: req.body,
      json: true
    }
    return createUser(options)
  }).catch(error => {
    context.res = {
      body: error
    }
  })

  context.res = {
    status: 200,
    body: result,
    headers: {
      'Content-Type': 'application/json'
    }
  }

}