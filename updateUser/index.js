const {getToken, patchUser} = require('./../graphHelper')
const secrets = require('../secrets')

module.exports = async function (context, req) {

  let tennantId = secrets.tenants.indexOf(req.query.tenant) > 0 ? req.query.tenant : secrets.tenants[0]

  let result = await getToken(tennantId).then(token => {

    let options = {
      method: 'PATCH',
      url: 'https://graph.microsoft.com/beta/users/' + req.query.id,
      headers:
        {
          'Authorization': token,
          'Content-Type': 'application/json'
        },
      body: req.body,
      json: true
    }
    return patchUser(options)
  }).catch(error => {
    context.res = {
      body: error
    }
  })

  context.res = {
    status: 200,
    body: req.body,
    headers: {
      'Content-Type': 'application/json'
    }
  }

}