const {getToken, getUser} = require('./../graphHelper')
const secrets = require('../secrets')

module.exports = async function (context, req) {

  let tennantId = secrets.tenants.indexOf(req.query.tenant) > 0 ? req.query.tenant : secrets.tenants[0]

  let result = await getToken(tennantId).then(token => {
    let options = {
      method: 'GET',
      url: 'https://graph.microsoft.com/beta/users/' + req.query.id,
      headers:
        {
          'Authorization': token
        }
    }
    return getUser(options)
  }).catch(error => {
    context.res = {
      body: error
    }
  })

  context.res = {
    body: result,
    headers: {
      'Content-Type': 'application/json'
    }
  }

}