const {getToken, patchUser, getUser} = require('./../graphHelper')
const secrets = require('../secrets')

const toBodyPatch = datauser => {
  return {
    givenName: datauser.givenName,
    surname: datauser.surname,
    city: datauser.city,
    country: datauser.country,
    department: datauser.department,
    displayName: datauser.displayName,
    jobTitle: datauser.jobTitle,
    mailNickname: datauser.mailNickname,
    mobilePhone: datauser.mobilePhone,
    officeLocation: datauser.mobilePhone,
    onPremisesImmutableId: datauser.onPremisesImmutableId,
    postalCode: datauser.postalCode,
    preferredLanguage: datauser.preferredLanguage,
    // state: datauser.state,
    streetAddress: datauser.streetAddress,
    usageLocation: datauser.usageLocation,
    // userPrincipalName: datauser.userPrincipalName,
    // userType: datauser.userType,
    showInAddressList : datauser.showInAddressList
  }
}

module.exports = async function (context, req) {
  if (!req.body.tenant || !req.body.id) {
    context.res = {
      status: 500,
      body: 'El id tenant y el id de usuario son requeridos !'
    }
    return
  }

  let tenantId = req.body.tenant
  let id = req.body.id

  let result = await getToken(tenantId).then(token => {
    let options = {
      method: 'GET',
      url: 'https://graph.microsoft.com/beta/users/' + id,
      headers:
        {
          'Authorization': token
        }
    }
    return getUser(options)
  })

  let dataUser = JSON.parse(result)

  let setTenants = secrets.tenants.filter(v => v != tenantId)

  for (let i = 0; i < setTenants.length; i++) {

    // obtener el usuario para usar su mail en el patch
    let userToUpdate = await getToken(setTenants[i]).then(token => {
      let param = dataUser.mail || dataUser.userPrincipalName
      let options = {
        method: 'GET',
        url: `https://graph.microsoft.com/v1.0/users?$filter=mail eq '${param}'`,
        headers:
          {
            'Authorization': token
          }
      }
      return getUser(options)
    })

    let objUser = JSON.parse(userToUpdate)

    // update del usuario
    await getToken(setTenants[i]).then(token => {

      let options = {
        method: 'PATCH',
        url: 'https://graph.microsoft.com/beta/users/' + objUser.value[0].id,
        headers:
          {
            'Authorization': token,
            'Content-Type': 'application/json'
          },
        body: toBodyPatch(dataUser),
        json: true
      }
      return patchUser(options)
    }).catch(error => {
      context.log('ERROR: ', error)
      context.res = {
        body: error
      }
    })
  }

  context.res = {
    status: 200,
    body: 'Sync tenants: ' + setTenants.join(' ')
  }

}