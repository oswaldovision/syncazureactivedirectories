const request = require('request')

const getOptionsInvite = function (displayName, email, tenant, idUser) {
  return {
    method: 'POST',
    url: `https://syncaadaval.azurewebsites.net/api/syncNewUser?code=wP0d1gmrDan2jrVlAit1Hop9qzlKdZp0bZp771TGKXQRJXQu2p5RdQ==&tenant=${tenant}`,
    headers:
      {
        'Content-Type': 'application/json'
      },
    body: {
      id: idUser,
      invitedUserDisplayName: displayName,
      invitedUserEmailAddress: email,
      invitedUserMessageInfo: {
        customizedMessageBody: 'Invitacion a Tennant',
        messageLanguage: 'string'
      },
      sendInvitationMessage: true,
      inviteRedirectUrl: 'https://invitations.microsoft.com/redeem',
      inviteRedeemUrl: 'https://visionsoftware.com'
    },
    json: true
  }
}

const getOptionsUpdate = function (tenant, id) {
  return {
    method: 'POST',
    url: 'https://syncaadaval.azurewebsites.net/api/syncUser?code=ZWvsyEdgpqAPNH5Ak1JKwMSJKsrEYWThad4LAN3ajFfFpPtCEMGqJQ==',
    headers:
      {
        'Content-Type': 'application/json'
      },
    body: {
      id,
      tenant
    },
    json: true
  }
}

const getOptionsDelete = function (tenant, upn) {
  return {
    method: 'POST',
    url: 'https://syncaadaval.azurewebsites.net/api/syncDeleteUser?code=ynBrtuhDbXALv/Sq2YWpxx/aKmF4M8Lvq0GFrJlGyhDAEcqa1J4saQ==',
    headers:
      {
        'Content-Type': 'application/json'
      },
    body:
      {
        tenant: tenant,
        upn: upn
      },
    json: true
  }
}

module.exports = function (context, eventHubMessages) {
  context.log(`JavaScript eventhub trigger function called for message array: ${eventHubMessages}`)

  eventHubMessages.forEach(function (message) {
    context.log(`Processed message: ${JSON.stringify(message)}`)
    var newProcess = message

    if (typeof newProcess.records != 'object') {
      context.log('input is an error process log, process only success process !')
      return
    }

    newProcess.records.forEach(function (objLog) {
      if ((objLog.operationName != 'Add user') && (newProcess.records[0].operationName != 'Update user') && (newProcess.records[0].operationName != 'Delete user')) {
        context.log('input without data to synchronize, only sync types: Add,Update and no have errors')
        return
      }

      var tenant = objLog.tenantId
      var email = objLog.properties.targetResources[0].userPrincipalName
      var idUser = objLog.properties.targetResources[0].id
      var displayName = objLog.properties.targetResources[0].userPrincipalName.split('@')[0]

      context.log('PROPS: ', tenant, email, displayName)

      let settingsRequest = {}
      if (objLog.operationName == 'Add user' && !displayName.includes('EXT')) {
        settingsRequest = getOptionsInvite(displayName, email, tenant, idUser)
      } else if (objLog.operationName == 'Update user' && !displayName.includes('EXT')) {
        settingsRequest = getOptionsUpdate(tenant, idUser)
      } else if(objLog.operationName == 'Delete user' && !displayName.includes('EXT')){
        let clearId = idUser.replace(/-/g,'')
        let clearUpn = email.replace(clearId,'')

        settingsRequest = getOptionsDelete(tenant, clearUpn)
      }else {
        context.log('no action: isn\'t add or update user')
        return
      }

      request(settingsRequest, function (error, response, body) {
        if (error) {
          context.log(error.message)
          return
        } else {
          context.log(body)
          return
        }

      })

    })

  })
  context.done()
}