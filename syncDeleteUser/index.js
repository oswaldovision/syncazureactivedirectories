const {getToken, getUser, deleteUser} = require('./../graphHelper')
const secrets = require('../secrets')

module.exports = async function (context, req) {
  context.log('JavaScript HTTP trigger function processed a request.')

  if (!req.body.tenant || !req.body.upn) {
    context.res = {
      status: 500,
      body: 'El id tenant y el upn son requeridos !'
    }
    return
  }

  let tenantId = req.body.tenant
  let upnToDelete = req.body.upn

  let setTenants = secrets.tenants.filter(v => v != tenantId)

  let success = []

  for (let i = 0; i < setTenants.length; i++) {

    // obtener el usuario para usar su mail en el delete
    let userToDelete = await getToken(setTenants[i]).then(token => {
      let options = {
        method: 'GET',
        url: `https://graph.microsoft.com/v1.0/users?$filter=mail eq '${upnToDelete}'`,
        headers:
          {
            'Authorization': token
          }
      }
      return getUser(options)
    })

    let objUser = JSON.parse(userToDelete)

    // Delete usuario
    await getToken(setTenants[i]).then(token => {

      let options = {
        method: 'DELETE',
        url: 'https://graph.microsoft.com/beta/users/' + objUser.value[0].id,
        headers:
          {
            'Authorization': token,
            'Content-Type': 'application/json'
          }
      }
      return deleteUser(options)
    }).then(result => {
      success.push(true)
    }).catch(error => {
      success.push(false)
      context.log('ERROR: ', error)
    })
  }

  if (success.every(value => {return value === true})) {
    context.res = {
      headers: {
        'Content-Type': 'application/json'
      },
      body: {'Sync tenants: Delete users in tenants: ': setTenants.join(' ')}
    }
  } else {
    context.res = {
      status: 500,
      body: 'Error Delete user !'
    }
  }
}