const {getToken, inviteUser, patchUser, getUser} = require('./../graphHelper')
const secrets = require('../secrets')

const toBodyPatch = datauser => {
  return {
    givenName: datauser.givenName,
    surname: datauser.surname,
    city: datauser.city,
    country: datauser.country,
    department: datauser.department,
    displayName: datauser.displayName,
    jobTitle: datauser.jobTitle,
    mailNickname: datauser.mailNickname,
    mobilePhone: datauser.mobilePhone,
    officeLocation: datauser.mobilePhone,
    onPremisesImmutableId: datauser.onPremisesImmutableId,
    postalCode: datauser.postalCode,
    preferredLanguage: datauser.preferredLanguage,
    // state: datauser.state,
    streetAddress: datauser.streetAddress,
    usageLocation: datauser.usageLocation,
    // userPrincipalName: datauser.userPrincipalName,
    // userType: datauser.userType,
    showInAddressList: datauser.showInAddressList
  }
}

module.exports = async function (context, req) {

  if (!req.query.tenant) {
    context.res = {
      status: 500,
      body: 'El id tenant es requerido !'
    }
    return
  }

  let tenantId = req.query.tenant

  let setTenants = secrets.tenants.filter(v => v != tenantId)

  let success = []

  //obtener los datos del user para luego actualizar
  let result = await getToken(tenantId).then(token => {
    let options = {
      method: 'GET',
      url: 'https://graph.microsoft.com/beta/users/' + req.body.id,
      headers:
        {
          'Authorization': token
        }
    }
    return getUser(options)
  })

  let dataUser = JSON.parse(result)

//loop de los tenant para crear el usuario de tipo guest
  for (let i = 0; i < setTenants.length; i++) {

    let tokenTenant = await getToken(setTenants[i]).then(token => {
      return token
    })

    let optionsToInvite = {
      method: 'POST',
      url: 'https://graph.microsoft.com/beta/invitations',
      headers:
        {
          'Authorization': tokenTenant,
          'Content-Type': 'application/json'
        },
      body: req.body,
      json: true
    }

    await inviteUser(optionsToInvite).then(user => {
      let optionsToPatch = {
        method: 'PATCH',
        url: 'https://graph.microsoft.com/beta/users/' + user.invitedUser.id,
        headers:
          {
            'Authorization': tokenTenant,
            'Content-Type': 'application/json'
          },
        body: toBodyPatch(dataUser),
        json: true
      }

      return patchUser(optionsToPatch)
    }).then(user => {
      success.push(true)
    }).catch(error => {
      success.push(false)
      context.log(`El Error ! ${error}`)
    })
  }

  if (success.every(value => {return value === true})) {
    context.res = {
      headers: {
        'Content-Type': 'application/json'
      },
      body: {'Sync tenants': setTenants.join(' ')}
    }
  } else {
    context.res = {
      status: 500,
      body: 'Error invited !'
    }
  }

}